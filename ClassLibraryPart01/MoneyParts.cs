﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryPart01
{
    public class MoneyParts
    {
        public string build(Decimal _input)
        {
            var _array = new Decimal[] { 0.05M, 0.1M, 0.2M, 0.5M, 1, 2, 5, 10, 20, 50, 100, 200 };
            var _list = new List<List<Decimal>>();
            var _output = "";

            foreach (var _item in _array)
            {
                var _arrayDecimal = new List<Decimal>();
                if (_input >= _item)
                {
                    if (_input % _item == 0)
                    {
                        var _ciclo = _input / _item;
                        for (int _i = 0; _i < _ciclo; _i++)
                        {
                            _arrayDecimal.Add(_item);
                        }
                    }
                    _list.Add(_arrayDecimal);
                }
            }

            foreach (var _data in _list) {
                if (_data.Count > 0)
                    _output += "[" + string.Join(",", _data) + "]";
            }

            return "[" + _output.Replace("][","],[") + "]";
        }
    }
}
