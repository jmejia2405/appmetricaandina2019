﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryPart01
{
    public class CompleteRange
    {
        public string build(string _input)
        {
            var _output = new List<int>();
            var _list_num = new List<int>();
            
            foreach (var _item in _input.Split(',')) {
                _list_num.Add(Int32.Parse(_item));
            }

            var _max = _list_num.Max();
            _output = Enumerable.Range(1, _max).ToList();

            return "[" + string.Join(",",_output) + "]";
        }
    }
}
