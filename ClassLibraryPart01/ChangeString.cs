﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryPart01
{
    public class ChangeString
    {
        public string Build(string _input) {
            var _output = "";
            var _abc = new List<char>() { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

            foreach (var _char in _input.ToCharArray()) {
                var _next = new char();
                if (IsNumeric(_char.ToString()) != true) { 
                    _next = (char.ToLower(_char) == 'z') ? 'a' : _abc[_abc.IndexOf(char.ToLower(_char)) + 1];
                    _output += (char.IsUpper(_char) == true) ? char.ToUpper(_next) : _next;
                }
                else
                    _output += _char;
            }
            return _output;
        }

        public static Boolean IsNumeric(string valor)
        {
            int result;
            return int.TryParse(valor, out result);
        }
    }
}
