﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibraryPart01;
namespace ConsoleAppPart01
{
    class Program
    {
        static void Main(string[] args)
        {
            var _ChangeString = new ClassLibraryPart01.ChangeString();
            var _CompleteRange = new ClassLibraryPart01.CompleteRange();
            var _MoneyParts = new ClassLibraryPart01.MoneyParts();
            var _valor = "";

            Console.WriteLine("----- Pregunta 01 ------");
            Console.Write("Ingrese valor: ");
            _valor = Console.ReadLine();
            Console.WriteLine("Resultado: " + _ChangeString.Build(_valor));
            Console.WriteLine("");
            Console.WriteLine("----- Pregunta 02 ------");
            Console.WriteLine("Formato #,#,#,...,n");
            Console.Write("Ingrese valor: ");
            _valor = Console.ReadLine();
            Console.WriteLine("Resultado: " + _CompleteRange.build(_valor));
            Console.WriteLine("");
            Console.WriteLine("----- Pregunta 03 ------");
            Console.WriteLine("Formato #.##");
            Console.Write("Ingrese valor: ");
            _valor = Console.ReadLine();
            Console.WriteLine("Resultado: " + _MoneyParts.build(Decimal.Parse(_valor)));
            Console.ReadKey();
        }
    }
}
